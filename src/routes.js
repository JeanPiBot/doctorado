/*Rutas de navegación */
import inicio from '@/components/inicio.vue'
import informacion from '@/components/nav/information.vue'
import profesores from '@/components/nav/profesores.vue'
import normatividad from '@/components/nav/normatividad.vue'
import diretesis from '@/components/nav/diretesis.vue'
import fechas from '@/components/nav/fechas.vue'

/*Rutas de profesores de primera corte y segunda corte */
import primeracohorte from '@/components/docen/primer.vue'
import segundacohorte from '@/components/docen/segundo.vue'

/*Rutas de somos*/
import presentacion from '@/components/somos/presentacion.vue'
import justificacion from '@/components/somos/justificacion.vue'
import perfiles from '@/components/somos/perfiles.vue'
import competencias from '@/components/somos/competencias.vue'

/*Rutas de Plan de estudios*/
import espacios from '@/components/planestu/espacios.vue'
import criterios from '@/components/planestu/criterios.vue'
import requisitos from '@/components/planestu/requisitos.vue'

/*Rutas de Investigación*/
import grupos from '@/components/investi/grupos.vue'
import lineas from '@/components/investi/lineas.vue'
import programas from '@/components/investi/programas.vue'
import productos from '@/components/investi/productos.vue'

/*Rutas de Internacionalización*/
import internacionalizacion from '@/components/internacio/internacionali.vue'


const routes = [
    {
        path: '/',
        component: inicio,
        name: '/'
    },
    {
        path: '/informacion',
        component: informacion,
        name: 'informacion'
    },
    {
        path: '/profesores',
        component: profesores,
        name: 'profesores'
    },
    {
        path: '/primeracohorte',
        component: primeracohorte,
        name: 'primeracohorte'
    },
    {
        path: '/segundacohorte',
        component: segundacohorte,
        name: 'segundacohorte'
    },
    {
        path: '/normatividad',
        component: normatividad,
        name: 'normatividad'
    },
    {
        path: '/direccionTesis',
        component: diretesis,
        name: 'diretesis'
    },
    {
        path: '/fechas',
        component: fechas,
        name: 'fechas'
    },
    {
        path: '/presentacion',
        component: presentacion,
        name: 'presentacion'
    },
    {
        path: '/justificacion',
        component: justificacion,
        name: 'justificacion'
    },
    {
        path: '/perfiles',
        component: perfiles,
        name: 'perfiles'
    },
    {
        path: '/competencias',
        component: competencias,
        name: 'competencias'
    },
    {
        path: '/espacios',
        component: espacios,
        name: 'espacios'
    },
    {
        path: '/criterios',
        component: criterios,
        name: 'criterios'
    },
    {
        path: '/requisitos',
        component: requisitos,
        name: 'requisitos'
    },
    {
        path: '/grupos',
        component: grupos,
        name: 'grupos'
    },
    {
        path: '/lineas',
        component: lineas,
        name: 'lineas'
    },
    {
        path: '/programas',
        component: programas,
        name: 'programas'
    },
    {
        path: '/productos',
        component: productos,
        name: 'productos'
    },
    {
        path: '/internacionalizacion',
        component: internacionalizacion,
        name: 'internacionalizacion'
    }
]

export default routes